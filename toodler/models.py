from django.db import models

# Create your models here.


class Tweet(models.Model):
	text = models.CharField(max_length=160)
	time = models.DateTimeField()

	def __unicode__(self):
		return self.text