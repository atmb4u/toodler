# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from toodler.models import Tweet
from datetime import datetime

def hello(request):
    return HttpResponse("Hello world ! ")

def home(request):
	if request.POST:
		text = request.POST['tweet']
		Tweet.objects.create(text=text, 
						time=datetime.now())
	tweets = Tweet.objects.all()
	c = {
	'tweets': tweets,
	}
	context = RequestContext(request, c)
	return render_to_response('index.html', 
								context)